import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac038 on 2017/5/8.
  */
object FilterDemoApp extends App{

  val conf = new SparkConf().setAppName{"FilterDemo"}
    .setMaster("local[*]")
  val sc = new SparkContext(conf)


  sc.textFile("./names.txt")
    .filter(name=>name.startsWith("M"))
    .foreach(println)

}
