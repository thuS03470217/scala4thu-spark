import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac038 on 2017/5/8.
  */
object WordCountApp extends App {

  val conf = new SparkConf().setAppName("WordCount").setMaster("local[*]")
  val sc = new SparkContext(conf)

  val lines = sc.textFile("/Users/mac038/Downloads/spark-doc.txt")
  val words = lines.flatMap(i => i.split(" "))

  words.groupBy(str => str).mapValues(_.size)
    .take(10)
    .foreach(println)
}
